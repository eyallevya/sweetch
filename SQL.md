SELECT 
COUNT(*),
GROUP_CONCAT(d.id) 
FROM data d 
JOIN lookup_ethnics l_e ON d.ethnic_code = l_e.code
JOIN lookup_sexes l_s ON d.sex_code = l_s.code
JOIN lookup_areas l_ar ON d.area_code = l_ar.code
JOIN lookup_ages l_ag ON d.age_code = l_ag.code 
JOIN lookup_years l_y ON d.year_code = l_y.code 
WHERE l_e.description = 'Asian' AND
l_s.description = 'Female' AND 
l_ar.description = 'Hampstead' AND
l_ag.description IN ('65 years and over','45-49 years','50-54 years','55-59 years','60-64 years','65-69 years','70-74 years','75-79 years','80-84 years','85-89 years','90-94 years','95-99 years','100 years and over','46 years','47 years','48 years','49 years','50 years','51 years','52 years','53 years','54 years','55 years','56 years','57 years','58 years','59 years','60 years','61 years','62 years','63 years','64 years','65 years','66 years','67 years','68 years','69 years','70 years','71 years','72 years','73 years','74 years','75 years','76 years','77 years','78 years','79 years','80 years','81 years','82 years','83 years','84 years','85 years','86 years','87 years','88 years','89 years','90 years','91 years','92 years','93 years','94 years','95 years','96 years','97 years','98 years','99 years','100 years','101 years','102 years','103 years','104 years','105 years','106 years','107 years','108 years','109 years','110 years','111 years','112 years','113 years','114 years','115 years','116 years','117 years','118 years','119 years','120 years and over') AND
l_y.description = '2018';