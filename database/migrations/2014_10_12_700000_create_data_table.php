<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            // $table->softDeletes();
            // $table->unsignedInteger('created_by');
            // $table->unsignedInteger('updated_by');
            $table->integer('year_code');
            $table->string('age_code');
            $table->integer('ethnic_code');
            $table->integer('sex_code');
            $table->string('area_code');
            $table->string('count')->nullable();
            // $table->foreign('year_code')->references('code')->on('lookup_years');
            // $table->foreign('age_code')->references('code')->on('lookup_ages');
            // $table->foreign('ethnic_code')->references('code')->on('lookup_ethnics');
            // $table->foreign('sex_code')->references('code')->on('lookup_sexes');
            // $table->foreign('area_code')->references('code')->on('lookup_areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}