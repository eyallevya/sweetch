<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLookupAgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_ages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            // $table->softDeletes();
            // $table->unsignedInteger('created_by');
            // $table->unsignedInteger('updated_by');
            $table->string('code');
            $table->string('description');
            $table->integer('sort_order');
            $table->index(['code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_ages');
    }
}