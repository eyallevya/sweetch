<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLookupYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lookup_years', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            // $table->softDeletes();
            // $table->unsignedInteger('created_by');
            // $table->unsignedInteger('updated_by');
            $table->integer('code');
            $table->string('description');
            $table->integer('sort_order');
            $table->index(['code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lookup_years');
    }
}