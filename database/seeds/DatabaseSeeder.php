<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LookUpYearsTableSeeder::class);
        $this->call(LookUpSexesTableSeeder::class);
        $this->call(LookUpEthnicsTableSeeder::class);
        $this->call(LookUpAreasTableSeeder::class);
        $this->call(LookUpAgesTableSeeder::class);
        $this->call(DataTableSeeder::class);
    }
}