<?php

use App\LookupSex;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class LookUpSexesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $full_name = storage_path('app/public/DimenLookupSex8277.csv');
        $file_handle = fopen($full_name, "r");

        while (($cell = fgetcsv($file_handle, 200, ",")) !== false) {
            // Skip header
            if (Str::lower($cell[0]) == 'code') {
                continue;
            }

            $lookup_sex = new LookupSex([
                'code' => $cell[0],
                'description' => $cell[1],
                'sort_order' => $cell[2],
            ]);

            $lookup_sex->save();
        }

        fclose($file_handle);
    }
}