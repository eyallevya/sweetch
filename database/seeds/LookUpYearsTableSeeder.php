<?php

use App\LookupYear;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class LookUpYearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $full_name = storage_path('app/public/DimenLookupYear8277.csv');
        $file_handle = fopen($full_name, "r");

        while (($cell = fgetcsv($file_handle, 200, ",")) !== false) {
            // Skip the header
            if (Str::lower($cell[0]) == 'code') {
                continue;
            }

            $lookup_year = new LookupYear([
                'code' => $cell[0],
                'description' => $cell[1],
                'sort_order' => $cell[2],
            ]);

            $lookup_year->save();
        }

        fclose($file_handle);
    }
}