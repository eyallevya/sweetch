<?php

use App\Data;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $full_name = storage_path('app/public/Data8277.csv');
        $file_handle = fopen($full_name, "r");

        while (($cell = fgetcsv($file_handle, 200, ",")) !== false) {
            // Skip header
            if (Str::lower($cell[0]) == 'year') {
                continue;
            }

            $data = new Data([
                'year_code' => $cell[0],
                'age_code' => $cell[1],
                'ethnic_code' => $cell[2],
                'sex_code' => $cell[3],
                'area_code' => $cell[4],
                'count' => $cell[5],
            ]);

            $data->save();
        }

        fclose($file_handle);
    }
}