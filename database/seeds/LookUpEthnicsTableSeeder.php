<?php

use App\LookupEthnic;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class LookUpEthnicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $full_name = storage_path('app/public/DimenLookupEthnic8277.csv');
        $file_handle = fopen($full_name, "r");

        while (($cell = fgetcsv($file_handle, 200, ",")) !== false) {
            // Skip header
            if (Str::lower($cell[0]) == 'code') {
                continue;
            }

            $lookup_ethnic = new LookupEthnic([
                'code' => $cell[0],
                'description' => $cell[1],
                'sort_order' => $cell[2],
            ]);

            $lookup_ethnic->save();
        }

        fclose($file_handle);
    }
}