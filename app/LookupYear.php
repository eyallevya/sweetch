<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LookupYear extends Model
{
    // use SoftDeletes;

    protected $fillable = ['code', 'description', 'sort_order'];

}