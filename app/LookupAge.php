<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LookupAge extends Model
{
    // use SoftDeletes;

    protected $fillable = ['code', 'description', 'sort_order'];
}