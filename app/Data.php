<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    // use SoftDeletes;

    protected $fillable = ['age_code', 'area_code', 'ethnic_code', 'sex_code', 'year_code', 'count'];

    /**
     * Get the Calling codes for the age.
     */
    public function age()
    {
        return $this->hasOne('App\Age', 'age_code', 'code');
    }

    /**
     * Get the Calling codes for the area.
     */
    public function area()
    {
        return $this->hasOne('App\Area', 'area_code', 'code');
    }

    /**
     * Get the Calling codes for the ethnic.
     */
    public function ethnic()
    {
        return $this->hasOne('App\Ethnic', 'ethnic_code', 'code');
    }

    /**
     * Get the Calling codes for the sex.
     */
    public function sex()
    {
        return $this->hasOne('App\Sex', 'sex_code', 'code');
    }

    /**
     * Get the Calling codes for the year.
     */
    public function year()
    {
        return $this->hasMany('App\Year', 'year_code', 'code');
    }
}