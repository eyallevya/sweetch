# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
** composer create-project laravel/laravel=5.7.* sweetch

* Configuration
* Dependencies
* Database configuration
** php artisan make:model -c -m LookupArea
** php artisan make:model -c -m LookupAge
** php artisan make:model -c -m LookupEthnic
** php artisan make:model -c -m LookupSex
** php artisan make:model -c -m LookupYear
** php artisan make:model -c -m Data

* Seeders
** php artisan make:seeder LookUpAgesTableSeeder
** php artisan make:seeder LookUpAreasTableSeeder
** php artisan make:seeder LookUpYearsTableSeeder
** php artisan make:seeder LookUpSexesTableSeeder
** php artisan make:seeder LookUpEthnicsTableSeeder
** php artisan make:seeder DataTableSeeder

* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact